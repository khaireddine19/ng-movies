import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  API_KEY:string = "affaf853793c006f5802746908885fd2"
  constructor(private http : HttpClient) { }

  getMovies() {
    return this.http.get('https://api.themoviedb.org/3/movie/top_rated?api_key='+this.API_KEY+'')
  }
  getMovieDetails(id:number) {
    return this.http.get("https://api.themoviedb.org/3/movie/"+id+"?api_key="+this.API_KEY+"")
  }
  getPopular() {
    return this.http.get('https://api.themoviedb.org/3/movie/popular?api_key='+this.API_KEY+'')
  }
}
