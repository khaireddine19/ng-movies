import { Component, Input, OnInit } from '@angular/core';
import { Movie } from 'src/app/models/movie';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {

  @Input() items: Movie[] = [];
  constructor() { }

  ngOnInit(): void {
  }

  title = 'ngSlick';

  slideConfig = {
    "slidesToShow": 1,
    "slidesToScroll": 1,
    "dots": false,
    // "autoplay": true,
    "infinite": true,
    "prevArrow": "<div class='nav-btn prev-slide'><span class='material-icons'>arrow_circle_left</span></div>",
    "nextArrow": "<div class='nav-btn next-slide'><span class='material-icons'>arrow_circle_right</span></div>",
  };

  slickInit(e: any) {
    console.log('slick initialized');
  }

  breakpoint(e: any) {
    console.log('breakpoint');
  }

  afterChange(e: any) {
    console.log('afterChange');
  }

  beforeChange(e: any) {
    console.log('beforeChange');
  }

}
