import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { MoviesService } from '../../services/movies.service'

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss']
})
export class MovieDetailsComponent implements OnInit {

  id:any = ""
  movie:any = []

  constructor(private route: ActivatedRoute, private moviesService: MoviesService) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id')
    this.getMovieDetails()
  }

  getMovieDetails() {
    this.moviesService.getMovieDetails(this.id).subscribe((response: any)=>{
      this.movie = response
    })
  }

}
