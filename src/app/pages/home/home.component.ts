import { Component, Input, OnInit } from '@angular/core';
import { MoviesService } from '../../services/movies.service';

import { Movie } from '../../models/movie'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {
  movies: Movie[] = [];
  popularMovies: Movie[] = [];

  constructor(private moviesService: MoviesService) { }
  ngOnInit(): void {
    this.getAllMovies()
    this.getPopularMovies()
  }

  getAllMovies() {
    this.moviesService.getMovies().subscribe((response: any)=>{
      this.movies = response.results
    })
  }

  getPopularMovies() {
    this.moviesService.getPopular().subscribe((response: any)=>{
      this.popularMovies = response.results
      console.log(this.popularMovies)
    })
  }

}
