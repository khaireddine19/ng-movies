import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { MovieDetailsComponent } from './pages/movie-details/movie-details.component';
import { MoviesComponent } from './pages/movies/movies.component';
import { SignInComponent } from './pages/sign-in/sign-in.component';

const routes: Routes = [
  { path:'', component: HomeComponent },
  { path:'movies', component: MoviesComponent },
  { path:'movies/:id', component: MovieDetailsComponent},
  { path:'sign-in', component: SignInComponent},
  { path: '**', redirectTo: '/' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
